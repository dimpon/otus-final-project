[![pipeline status](https://gitlab.com/dimpon/otus-final-project/badges/master/pipeline.svg)](https://gitlab.com/dimpon/otus-final-project/-/commits/master)

# Loguno applicaion development platform 

This project is a final task of training "[Infrastructure Platform base on Kubernetes](https://otus.ru/lessons/infrastrukturnaya-platforma-na-osnove-kubernetes/?int_source=courses_catalog&int_term=operations)"

The platform helps development team to develop application (Loguno), support testing, packaging, delivery. 

The pet project Loguno contains 2 microservices, are written on Java, Spring Boot: 
![image](pictures/loguno.png)

Every developer works in his own branch, name starting from "feature*". Then, using commands in commit message developer can build and deploy branch content to dedicated namespace in K8s cluster.

Application is available by URL: _branch hash_.loguno.org. Namespace has TTL. TTL supported by [kube-janitor](https://github.com/hjacobs/kube-janitor).
 
Master branch is deployed into permanent namespace and presents the most up-to-date version of the product.

#### Project structure:
* Application. Contains application and Dockerfiles for build. Each microservice is built to separate image and pushed to Docker Hub. Folder contains docker-compose file for local running.
* Cluster. Contains scripts for preparing K8s cluster. Current implementation uses GKE, but most scripts can be used on Bare Metal.
* Deploy. Contains Helm charts for deploying application to K8s cluster.

#### Commit message commands:

|   	 |   	|
|---	 |---	|
| _pre   | Runs pre-job which logs values of build   	|
| _build | Build application, package into Docker images nd push to Docker Hub  	|
| _deploy| Deploy application into K8s cluster to loguno-commit hash namespace  	|

Project is hosted on GitLab and uses gitlab-ci, the full pipeline is:
 ![image](pictures/pipeline.png)
 
As mentioned, each developer works in own sandbox and does not touch others. When he commits and pushes with commit message containing: 
``_pre _build _deploy`` 
Pipeline:
* Creates a branch unique ID (basically it is a hash from branch name); 
* Builds docker images, with tag + unique branch ID;
* Pushes all images to Docker hub;
* Using Helm charts deploys in namespace loguno-<branch ID> and expose endpoint on https://ID.loguno.org   

When the same is done in master branch:
* Pipeline waits for manual run;
* No unique ID;
* Builds docker images, with tag;
* Pushes all images to Docker hub;
* Using Helm charts deploys in namespace loguno-master and expose endpoint on https://master.loguno.org   
  
![image](pictures/build-deploy.png) 
 
#### Infrastructure services  
* Endpoints are secured with [cert-manager](https://cert-manager.io/) using [letsencrypt](https://letsencrypt.org/) certificates. 
* For monitoring Kubernetes dashboard is installed [https://kubernetes-dashboard.loguno.org/](https://kubernetes-dashboard.loguno.org/)
```{r}
eyJhbGciOiJSUzI1NiIsImtpZCI6IkhUXzRYSTh6Y182cUZtNVQ2VzVhc3d3cFRITmY3aUtnN0xHcWhYNmNDZm8ifQ.eyJpc3MiOiJrdWJlcm5ldGVzL3NlcnZpY2VhY2NvdW50Iiwia3ViZXJuZXRlcy5pby9zZXJ2aWNlYWNjb3VudC9uYW1lc3BhY2UiOiJrdWJlcm5ldGVzLWRhc2hib2FyZCIsImt1YmVybmV0ZXMuaW8vc2VydmljZWFjY291bnQvc2VjcmV0Lm5hbWUiOiJhZG1pbi11c2VyLXRva2VuLWRmNjR4Iiwia3ViZXJuZXRlcy5pby9zZXJ2aWNlYWNjb3VudC9zZXJ2aWNlLWFjY291bnQubmFtZSI6ImFkbWluLXVzZXIiLCJrdWJlcm5ldGVzLmlvL3NlcnZpY2VhY2NvdW50L3NlcnZpY2UtYWNjb3VudC51aWQiOiIxNWY4ZDIzZS1jZjgzLTRkOWItYjgxZi1jYzQzNTQzNjM3ZTUiLCJzdWIiOiJzeXN0ZW06c2VydmljZWFjY291bnQ6a3ViZXJuZXRlcy1kYXNoYm9hcmQ6YWRtaW4tdXNlciJ9.vHKCiBRrrnM5lPn3nqtMzJ7AXRVa64cbQxj8A4hZxBMmHnWn4jWouecyAqhVMYuqhzkp_WAeY4EB-WUpDTkF-QPrgsr4HlnS63efP64tlNFBo4oX3cMMPvACVII-F8b7fdlWtq4XGgpJGxrWr2pCgdcLrZPqEhKTmo2nzi0wDvjwKsDXta3NAi2VW207gpxwneSAHv3becXPfPujICrWZfWXn-PNkYwPlipv1NHKUa5H5WolRM9P5Y570UdiUy5BTC9ROkAwcpoCWK-XG_ZIsKOWlrup5bXHXju_FTjpb3Cb6RBoXa5KjduziGbR5-0LHUwjBQznOQv1JZVq8b2Eqg
```
* Elasticsearch collects logs, Kibana is logs aggregator [https://kibana.loguno.org/](https://kibana.loguno.org/)
* Prometheus collects cluster and loguno metrics [https://prometheus.loguno.org/graph](https://prometheus.loguno.org/graph). 
* Grafana can be used for Dashbords building [https://grafana.loguno.org](https://grafana.loguno.org) (xxxxx)

#### Cluster structure
 Cluster has 2 nodes pool - for infrastructure services and for application. 
 For distibuting pods ``nodeSelector`` is used.
 ![image](pictures/cluster-structure.png)

#### Next steps:
* ~~Install central logs collection system (ELK)~~
* ~~Monitor metrics with Prometheus~~
* Install Harbor, create retention policy for images
* ~~Cert-manager for securing endpoints.~~




