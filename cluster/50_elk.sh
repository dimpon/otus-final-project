#!/bin/bash

helm repo add elastic https://helm.elastic.co
helm repo add fluent https://fluent.github.io/helm-charts

kubectl create ns observability

helm upgrade --install elasticsearch elastic/elasticsearch --namespace observability -f ./elk/elasticsearch.values.yaml
#install metrics exporter
#helm upgrade --install elasticsearch-exporter stable/elasticsearch-exporter --set es.uri=http://elasticsearch-master:9200 --set serviceMonitor.enabled=true --namespace=observability --version=3.7.1
#helm upgrade --install prometheus-elasticsearch-exporter prometheus-community/prometheus-elasticsearch-exporter --set es.uri=http://elasticsearch-master:9200 --set serviceMonitor.enabled=true --namespace=observability --version=3.7.1

helm install prometheus-elasticsearch-exporter prometheus-community/prometheus-elasticsearch-exporter  --set es.uri=http://elasticsearch-master:9200 --set serviceMonitor.enabled=true --namespace=observability

helm upgrade --install kibana elastic/kibana --namespace observability -f ./elk/kibana.values.yaml
helm upgrade --install fluent-bit stable/fluent-bit --namespace observability -f ./elk/fluent-bit.values.yaml --version=2.10.2

#helm delete elasticsearch --namespace observability
#helm delete kibana --namespace observability
#helm delete fluent-bit --namespace observability
#helm upgrade --install fluent-bit stable/fluent-bit --namespace observability -f ./elk/fluent-bit.values.yaml
#helm upgrade --install fluent-bit fluent/fluent-bit --namespace observability -f ./elk/fluent-bit.values.yaml

#Loki
helm repo add loki https://grafana.github.io/loki/charts
helm repo update
helm upgrade --install loki loki/loki-stack --namespace observability -f ./elk/loki.values.yaml
