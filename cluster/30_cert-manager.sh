#!/bin/bash
kubectl create ns cert-manager
kubectl label namespace cert-manager certmanager.k8s.io/disable-validation="true"

helm repo add jetstack https://charts.jetstack.io

helm upgrade --install \
  cert-manager jetstack/cert-manager \
  --namespace cert-manager \
  --version v1.0.1 \
  --set installCRDs=true \
  --set nodeSelector.purpose=infra

sleep 10

kubectl apply -f ./cert-manager/00_cluster-issuer.yaml