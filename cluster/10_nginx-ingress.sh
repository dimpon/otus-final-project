#!/bin/bash
kubectl create ns nginx-ingress
helm repo add nginx https://helm.nginx.com/stable
helm repo update
helm upgrade --install nginx-ingress nginx/nginx-ingress --wait --namespace=nginx-ingress --version=0.6.1 -f ./nginx-ingress/nginx-ingress.values.yaml

