#!/bin/bash
gcloud config set project glowing-space-280619

gcloud config set compute/zone europe-west4-a

gcloud container clusters create \
  --zone europe-west4-a \
  --cluster-version latest \
  otus-final-project-cluster


gcloud container node-pools delete default-pool --cluster=otus-final-project-cluster

gcloud beta container clusters update \
  otus-final-project-cluster \
  --logging-service none \
  --monitoring-service none

gcloud container node-pools create app-pool --cluster otus-final-project-cluster \
  --node-labels=purpose=app \
  --machine-type e2-standard-2 \
  --num-nodes 1

gcloud container node-pools create infra-pool --cluster otus-final-project-cluster \
  --node-labels=purpose=infra \
  --machine-type e2-standard-4 \
  --num-nodes 2