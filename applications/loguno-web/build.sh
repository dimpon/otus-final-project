#!/bin/bash

TAG="v0.0.1"
docker build --build-arg PORT=8080 -t dimpon/loguno-web:$TAG .
docker push dimpon/loguno-web:$TAG

#docker run -p 8080:8080 -d -e SERVICE_ADDR=service:9000 dimpon/fp-web:$TAG
