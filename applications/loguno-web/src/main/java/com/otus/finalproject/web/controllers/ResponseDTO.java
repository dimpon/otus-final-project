package com.otus.finalproject.web.controllers;

import lombok.Data;

@Data
public class ResponseDTO {
    private final String message;
    private final long timestamp;
}
