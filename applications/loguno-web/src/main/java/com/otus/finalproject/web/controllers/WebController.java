package com.otus.finalproject.web.controllers;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Slf4j
@Controller
public class WebController {

    @Value("${application.version}")
    private String version;

    @Autowired
    private ServiceClient serviceClient;

    @RequestMapping(method = RequestMethod.GET)
    public String index(Model model) {
        model.addAttribute("version", version);
        return "index";
    }

    @RequestMapping(value = "service1", method = RequestMethod.POST)
    public String service1(Model model) {
        log.info("service1");
        ResponseDTO responseDTO = serviceClient.service1();
        log.info("service1 {}", responseDTO);
        model.addAttribute("version", version);
        model.addAttribute("response",responseDTO);
        return "index";
    }

    @RequestMapping(value = "service2", method = RequestMethod.POST)
    public String service2(Model model) {
        log.info("service2");
        ResponseDTO responseDTO = serviceClient.service2();
        log.info("service2 {}", responseDTO);
        model.addAttribute("version", version);
        model.addAttribute("response",responseDTO);
        return "index";
    }

    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR,
            reason = "Internal Server error")
    @ExceptionHandler(value = {Exception.class, RuntimeException.class})
    protected void handleException(Throwable exception) {
        log.error("Exception on UI", exception);
    }
}
