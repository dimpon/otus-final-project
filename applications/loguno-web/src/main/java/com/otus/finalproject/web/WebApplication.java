package com.otus.finalproject.web;

import com.otus.finalproject.web.controllers.ServiceClient;
import feign.Feign;
import feign.Logger;
import feign.gson.GsonDecoder;
import feign.gson.GsonEncoder;
import feign.okhttp.OkHttpClient;
import feign.slf4j.Slf4jLogger;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@SpringBootApplication
public class WebApplication {

    public static void main(String[] args) {
        SpringApplication.run(WebApplication.class, args);
    }

    @Slf4j
    @Configuration
    public static class WebConfiguration {

        @Value("${application.service.address}")
        private String serviceAddress;


        @Bean
        public ServiceClient serviceClient() {
            log.info("Service addr: {}",serviceAddress);
            return Feign.builder()
                    .client(new OkHttpClient())
                    .encoder(new GsonEncoder())
                    .decoder(new GsonDecoder())
                    .logger(new Slf4jLogger(ServiceClient.class))
                    .logLevel(Logger.Level.FULL)
                    .target(ServiceClient.class, "http://" + serviceAddress);
        }
    }

}
