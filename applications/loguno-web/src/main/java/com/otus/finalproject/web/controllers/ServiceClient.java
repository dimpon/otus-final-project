package com.otus.finalproject.web.controllers;

import feign.RequestLine;

public interface ServiceClient {
    @RequestLine("GET /service1")
    ResponseDTO service1();

    @RequestLine("GET /service2")
    ResponseDTO service2();
}
