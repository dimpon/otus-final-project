package com.otus.finalproject.service;

import io.micrometer.core.annotation.Timed;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.MDC;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@SpringBootApplication
@RestController
@Timed("loguno-backend")
public class ServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(ServiceApplication.class, args);
    }

    @RequestMapping(value = "/service1", method = RequestMethod.GET)
    public ResponseEntity<ResponseDTO> service1() {
        try {
            MDC.put("user", "john doe");
            log.info("Call {}", "service1");
            return new ResponseEntity<>(new ResponseDTO("service1", System.currentTimeMillis()), HttpStatus.OK);
        } finally {
            MDC.clear();
        }
    }

    @RequestMapping(value = "/service2", method = RequestMethod.GET)
    public ResponseEntity<ResponseDTO> service2() {
        try {
            MDC.put("user", "joanne doe");
            log.info("Call {}", "service2");
            return new ResponseEntity<>(new ResponseDTO("service2", System.currentTimeMillis()), HttpStatus.OK);
        } finally {
            MDC.clear();
        }
    }

    @Data
    public static class ResponseDTO {
        private final String message;
        private final long timestamp;
    }


}
