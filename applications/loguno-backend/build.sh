#!/bin/bash

TAG="v0.0.1"
docker build  -t dimpon/loguno-backend:$TAG .
#docker push dimpon/loguno-backend:$TAG

docker run -p 9000:9000 -d  dimpon/loguno-backend:$TAG
